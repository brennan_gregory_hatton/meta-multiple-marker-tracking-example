﻿/*MutipleMarkerScript by Brennan Hatton - SiliconVagabond.com
 * 
 *		This script moves the target object based on which marker it sees.
 *		You can use this script to have mutliple markers to track one object, for redundancy. However, it does not increase stablization. It will only track fmr one of the markers it see's. Perhaps I will try and add stabalization later.
 *		This is best used for navigating around large objects, where one marker will not be enough to track it.
 *
 *		You can also use this script to reposition the MetaFrame (the glasses) based on which marker you are looking at. It is important to check the Move Camera check box in the inspector if you set the targetObject to MetaFrame. As an external script can not modify the MetaFrames rotation, and attempting to do this would not get good results.
 *
 *
 *		This script moves the targetObject depending on which marker it is looking at.
 *		It has two publc variables that need to be set in the inspector
 *		 - The first is a reference to the object you are looking at to be moved "targetObject"
 *		- The secon is a list of MarkerLocations, which are made up of Marker ID's and a GameObject to represent the position of the marker
 *		To use this class effectively, create as many GameObjects as you have markers, position the in your virtual world relative to where 
 *		your markers are positioned in the real world. Then give this class a reference to those markers in the MarkerLocations List in the 
 * inspector and assign the ID's of each marker
 * 
 * */

using UnityEngine;
using System.Collections;
using Meta;// Required to access the MetaWorld
using System.Collections.Generic;//Required to use Lists

public class MutipleMarkerScript : MonoBehaviour {
	
	//This determins if we move the camera or the obejcts based on the marker we see
	public bool MoveCamera = true;
	
	//a reference to the object we want to move around
	public GameObject targetObject; //We make it public so we can set it in the inspector
	
	//a reference to the MetaBody Component
	private MetaBody metaBodyComponent;

	//Marker Locations connect GameObjects to Markers	
	[System.Serializable]//System.Serializable makes the List editable from the inspector
	public class MarkerLocation{
		
		//The Gameobject that holds the position in space for your marker
		public GameObject PositionObject;//This an be replaced with a Vector3 if you would rather just enter the position in the inspector

		//The ID of the marker that you want connected the position of the above Game Object
		public int MarkerID;
		
	}
	
	//List of Markers ID's and positions
	public List<MarkerLocation> RelativeMarkerPositions;
	
	[HideInInspector]
	//List of Markers ID's and positions
	public List<GameObject> WorldMarkerPositions = new List<GameObject>();

	// Use this for initialization
	void Start () {
		
		//Add the MetaBody component to this game object
		metaBodyComponent = this.gameObject.AddComponent<MetaBody> ();
		
		//Enable Marker Tracking!
		metaBodyComponent.markerTarget = true;
		
		//Set the Marker ID we are looking for
		metaBodyComponent.markerTargetID = 1; //1 is the id of the marker on our welcome card
		
		//Turn off the marker target indicator
		MarkerDetector.Instance.gameObject.GetComponent<MarkerTargetIndicator> ().enabled = true;//false;
		
		if (!MoveCamera)
		{
			
			//If we are moving the objects based on the marker positions
			//.. we will need to create objects to stick to each marker
			//.. than we can parent the targetObject to the first visible marker
			
			//Lets create an object ot parent them to so we dont have them all floating around
			GameObject markerParent = new GameObject();
			markerParent.name = "Marker Parent";
			
			for(int i = 0; i < RelativeMarkerPositions.Count; i++)
			{
				//Create a game object
				GameObject markerObject = new GameObject();
				
				//name it based on the ID
				markerObject.name = RelativeMarkerPositions[i].MarkerID.ToString();
				
				//Add a MetaBody
				MetaBody metaBody = markerObject.AddComponent<MetaBody>();
				
				//Turn on marker tracking
				metaBody.markerTarget = true;
				
				//Set marker ID
				metaBody.markerTargetID = RelativeMarkerPositions[i].MarkerID;
				
				//set parent
				markerObject.transform.SetParent(markerParent.transform);
				
				//Add to list of world marker positions
				WorldMarkerPositions.Add(markerObject);
				
				
			}
		}
		
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (MoveCamera)
		{
			
			MoveCameraToMarker();
		}else
		{
			MoveObjectToMarkers();
		}
	}
	
	void MoveCameraToMarker()
	{
		//Check for each marker
		foreach (MarkerLocation marker in RelativeMarkerPositions) 
		{
			//Can we see the marker?
			if (LookForMarker (marker.MarkerID))
			{
				//Yes - move the MetaFrame
				targetObject.transform.position = marker.PositionObject.transform.position;
				
			}
		}
	}

	//Check the marker we are looking for is in sight of the camera
	bool LookForMarker(int id)
	{
		//Check that markers are in use
		if (MarkerDetector.Instance != null) {
			
			//Check if the marker we want exists
			return (MarkerDetector.Instance.updatedMarkerTransforms.Contains(id));
		}
		
		//We count not find it
		return false;
	}
	
	int trackingMarker = -1;
	
	void MoveObjectToMarkers()
	{
		//if tracking marker
		if (trackingMarker != -1)
		{
			//check that marker can be seen
			if (LookForMarker(trackingMarker))
			{
				//Good keep tracking
				
			}
			else
			{
				//Stop Tracking
				targetObject.transform.parent = null;
				
				//update trakcing switch
				trackingMarker = -1;
				
				
			}
		}else
		{
			//Check if any markers are tracking
			if (MarkerDetector.Instance.updatedMarkerTransforms.Count > 0)
			{
				
					//Check for each marker in list
				for(int i = 0; i < RelativeMarkerPositions.Count; i++)
				{
						//if that marker is found
					if (LookForMarker(RelativeMarkerPositions[i].MarkerID))
					{
						
							//Track that marker
						trackingMarker = i;
						
							//Parent object ot marker
						targetObject.transform.SetParent(WorldMarkerPositions[i].transform);
						
						Debug.Log(WorldMarkerPositions[i]);
						
							//Set realitve offset
						targetObject.transform.localPosition = RelativeMarkerPositions[i].PositionObject.transform.localPosition;
						
						targetObject.transform.localRotation = Quaternion.identity;
						
							//Stop looking
						i = RelativeMarkerPositions.Count;
					}
				}
			}
		}
			
	}
}
