# README #

## MutlipleMarkersOnOneObject Scene ##

This scene places a mountain on your table about 1m width x 1m length and uses multiple markers so you can look around it and don't have to worry about sticking to only 1 marker. !! It does not improve stability !! But maybe you can add that for me?

Setup like so:
- Using marker tracking of 5 markers.
![markerSettup.PNG](https://bitbucket.org/repo/qrMG7x/images/2853997855-markerSettup.PNG)

### What is this repository for? ###

* Example of how markers can be used for trakcing mutliple at a time.

### How do I get set up? ###

* Install MetaSDK. Currently running off MetaSDK1.3.3.
* Install Unity 5.2.2f1 (recommended version for MetaSDK1.3.3)
* more detials on setup here: http://www.siliconvagabond.com/meta-multiple-marker-tracking/



### Who do I talk to? ###

* Brennan Hatton @ SiiconVagabond.com - brennan@siliconvagabond.com